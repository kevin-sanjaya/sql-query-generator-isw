package com.sqlQueryGenerator.isw;

import com.opencsv.CSVReader;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.BadLocationException;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.UUID;

public class IswApplication {
	public static JTextArea initiateAppFrame() {
		JPanel middlePanel = new JPanel();
		middlePanel.setBorder(new TitledBorder(new EtchedBorder(),"DIM Merchant Onboarding"));
		JTextArea display = new JTextArea (50,120 );
		display.setEditable(false);
		JScrollPane scroll = new JScrollPane (display);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		middlePanel.add(scroll);
		JFrame frame = new JFrame();
		frame.add(middlePanel);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		return display;
	}

	public static void main(String[] args) throws InterruptedException, BadLocationException {
		JTextArea display = initiateAppFrame();

		if (new File("input.csv").exists()) {
			display.append("input.csv file is found.\n\n");
			String csvFile = "input.csv";
			CSVReader reader = null;
			try {
				reader = new CSVReader(new FileReader(csvFile));
				String[] line;

				display.append("---QUERY FOR NEW MERCHANT ONBOARDING---\n");
				ArrayList<String> uuidList = new ArrayList<>();
				while ((line = reader.readNext()) != null) {
					if (line.length != 4 && line.length != 1)
						throw new Exception();

					else if (line.length != 1) {
						String uuid = UUID.randomUUID().toString();
						display.append("INSERT INTO YOUR_CONFIGURATION_DB_NAME.organization (id, name, type, registered_business_name, external_id) VALUES ('"+ uuid +"', '"+line[0]+"', 'MERCHANT', '"+line[1]+"', '"+line[2]+"');\n");
						uuidList.add(uuid);
					}

				}
				reader = new CSVReader(new FileReader(csvFile));
				display.append("\n---QUERY FOR NEW USER ADMIN ONBOARDING---\n");

				int index = 0;
				while ((line = reader.readNext()) != null) {
					if (line.length != 4 && line.length != 1)
						throw new Exception();

					else if (line.length != 1) {
						display.append("INSERT INTO YOUR_CONFIGURATION_DB_NAME.user_invitation (id, email_address, organization_id, role) VALUES ('"+UUID.randomUUID()+"', '"+line[3]+"', '"+uuidList.get(index)+"', 'MERCHANT_ADMIN');\n");
						index++;
					}
				}
			} catch
			(Exception e) {
				display.setText("");
				display.append("There is something wrong with the content of input.csv file. Make sure to follow the correct format.");
				e.printStackTrace();
			}

		} else {
			display.append("input.csv file is not found. Make sure file input.csv is in the same folder with this application.");
		}
	}
}
